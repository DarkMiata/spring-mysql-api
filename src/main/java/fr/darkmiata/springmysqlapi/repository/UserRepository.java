package fr.darkmiata.springmysqlapi.repository;

import fr.darkmiata.springmysqlapi.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
}
