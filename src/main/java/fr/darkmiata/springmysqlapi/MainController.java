package fr.darkmiata.springmysqlapi;

import fr.darkmiata.springmysqlapi.models.User;
import fr.darkmiata.springmysqlapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
public class MainController {

	@Autowired
	private UserRepository userRepository;

	public static void main(String[] args) {
		System.out.println("main lancé !");
	}
}
