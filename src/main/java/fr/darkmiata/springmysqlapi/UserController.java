package fr.darkmiata.springmysqlapi;

import fr.darkmiata.springmysqlapi.models.User;
import fr.darkmiata.springmysqlapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/demo/v2/user")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@GetMapping(path = "/add")
	public @ResponseBody String addNewUser(@RequestParam String name, @RequestParam String email) {
		User u = new User();

		u.setName(name);
		u.setEmail(email);
		userRepository.save(u);

		return "saved";
	}

	@GetMapping(path = "/all")
	public @ResponseBody Iterable<User> userList() {
		return userRepository.findAll();
	}
}
